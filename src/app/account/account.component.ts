import {Component, OnInit} from '@angular/core';
import {AccountService} from "../AccountService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  name: string;

  constructor(private ac: AccountService) {

  }

  ngOnInit() {
    this.ac.getInfo().subscribe(data => {
      this.name = data.username;
    }, err => {
      console.log(err);
      this.name = "Test";
    });

  }


}
