/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {HomeComponent} from './home.component';
import {LoginComponent} from "../login/login.component";
import {RegisterComponent} from "../register/register.component";
import {Router} from "@angular/router";

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let mockRouter = {
    navigate: jasmine.createSpy('room')
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent, LoginComponent, RegisterComponent],
      providers: [
        {provide: Router, useValue: mockRouter}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it(`should have as title 'Stadspel Sint-Niklaas'`, async(() => {
    let fixture = TestBed.createComponent(HomeComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Stadspel Sint-Niklaas');
  }));

  it('should render title in a h1 tag', async(() => {
    let fixture = TestBed.createComponent(HomeComponent);
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Stadspel Sint-Niklaas');
  }));

  it('p should have text', async(() => {
    let fixture = TestBed.createComponent(HomeComponent);
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent);
  }));
});
