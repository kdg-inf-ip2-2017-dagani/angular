import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {observableToBeFn} from "rxjs/testing/TestScheduler";
import {ConnectionAccesPoints} from "./ConnectionAccesPoints";
/**
 * Service for organising the room and the actual game
 * Created by Oliver on 09/02/2017.
 */

@Injectable()
export class RoomService {
  private apiURL = ConnectionAccesPoints._apiURL;

  constructor(private http: Http) {
  }

  createRoom(json): Observable<any> {
    console.log(json);
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post(this.apiURL + "/games", json, options).map(this.extractData).catch(this.handleError);
  }

  getGame(id: string) {
    return this.http.get(this.apiURL + '/games/' + id).map(res => res.json()).catch(this.handleError);
  }

  startGame(id: string, token: string) {
    console.log('started');
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post(this.apiURL + '/games/' + id + '/startGame/' + token, null, options).map(this.extractData).catch(this.handleError);
  }

  stopGame(id: string, token: string) {
    console.log('stoped');
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post(this.apiURL + '/games/' + id + '/stopGame/' + token, null, options).map(this.extractData).catch(this.handleError);
  }
  updateGame(json: any, token: string) {
    console.log(json);
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post(this.apiURL + '/games/updategame/' + token, json, options).map(this.extractData).catch(this.handleError);
  }

  registerSocket(id: string, token: string) {
    console.log('connecting to ' + this.apiURL + '/games/' + id + '/webreg/' + token);
    return this.http.get(this.apiURL + '/games/' + id + '/webreg/' + token).map(this.extractData).catch(this.handleError);
  }

  /**
   * text() ,not json() because private attributes
   * @param res
   * @returns {string}
   */
  private extractData(res: Response) {
    console.log(res);
    let body = res.text();//
    console.log(body);
    return body;
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      errMsg = error.status.toString();
    } else {
      errMsg = "error";
    }
    return Observable.throw(errMsg);
  }


}
