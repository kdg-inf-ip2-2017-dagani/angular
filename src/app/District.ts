import {SebmGoogleMap, LatLngLiteral} from 'angular2-google-maps/core';

export class District {
  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }
  get plein(): boolean {
    return this._plein;
  }

  set plein(value: boolean) {
    this._plein = value;
  }

  get name(): string {
    return this._name;
  }
  private _id:string;
  private _name: string;
  private _polypoints: Array<LatLngLiteral>;
  private _color: string;
  private _middle: LatLngLiteral;
  private _plein: boolean;
  private _icon: string;

  constructor(id:string,name: string) {
    this._id=id;
    this._name = name;
    this._polypoints = [];
    this._color = "#FFF";
    this._plein = false;
    this._icon = "";
  }

  addPoint(xlat, ylng) {
    this._polypoints.push({lat: xlat, lng: ylng});
  }

  setmiddle(xlat, ylng) {
    this._plein = true;
    if(this.name.charAt(1)==='A'){
      this._icon = "assets/treasure.png";
    }else{
      this._icon="assets/pleinpng.png";;
    }
    this._middle = {lat: xlat, lng: ylng};
  }


//
  get polypoints(): Array<LatLngLiteral> {
    return this._polypoints;
  }

  get color(): string {
    return this._color;
  }

  get middle(): LatLngLiteral {
    return this._middle;
  }

  get hoofdplein() {
    return false;
  }


  get icon() {
    return this._icon;

  }

  set color(value: string) {
    this._color = value;
  }
}
