import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AccountService} from "../AccountService";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  logingroup: FormGroup;

  constructor(private router: Router, private ac: AccountService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.logingroup = this.fb.group({
      emailaddress: ['', Validators.required],
      password: ['', Validators.required]
    })

  }

  public login({value, valid}:{value: any, valid: boolean}) {
    if (valid) {
      this.ac.login(value).subscribe(() => {
        this.router.navigate(['/room']);
      }, () => {
        alert('ongeldige inlog gegevens')
      })
    }
  }

}
