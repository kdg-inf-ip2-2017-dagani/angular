/**
 * Created by Toon Vermeulen on 3/13/2017.
 */
import {Player} from "./Player";
export class PlayersMap {
  get players(): Array<Player> {
    return this._players;
  }

  constructor() {
  }

  private _players: Array<Player> = [];
  index: Array<any> = [];

  add(p: Player) {
    this._players.push(p);
    this.index[p.id] = this._players.indexOf(p);
  }

  get(id: string):Player {
    return this._players[this.index[id]];
  }
}


