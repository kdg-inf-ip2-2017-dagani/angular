import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {RoomService} from "../RoomService";
import {Router} from "@angular/router";
import {AccountService} from "../AccountService";

@Component({
  selector: 'app-start-settings',
  templateUrl: './start-settings.component.html',
  styleUrls: ['./start-settings.component.css'],
  providers: [RoomService]
})
export class StartSettingsComponent implements OnInit {
  private settingsgroup: FormGroup;
  private data: any;
  private sub: any;

  constructor(public fb: FormBuilder, private service: RoomService, private router: Router, private ac: AccountService) {
  }

  ngOnInit() {
    this.settingsgroup = this.fb.group({
      maxPlayersPerTeam: [2],
      maxTeams: [2],
      roomName: ['', Validators.required],
      hostPassword: ['', Validators.required]
    });
  }

  public onSubmit({value, valid}:{value: any, valid: boolean}) {
    if (valid) {
      let t = this.ac.token;
      let token = "{\"token\": \"" + t + "\",";
      let json = JSON.stringify(this.settingsgroup.value);
      console.log(json);
      this.sub = this.service.createRoom(token + json.substr(1));
      this.sub.subscribe(data => {
          console.log(data);
          this.route(data);
        }, err => {
          // Log errors if any
          console.log(err);
          if (err > 400) {
            this.errRoute();
          }
        }
      );
    }
  }

  private errRoute() {
    this.router.navigate(['/'])
  }

  private route(data) {
    console.log(data);
    this.router.navigate(['/room', data]);
  }
}
