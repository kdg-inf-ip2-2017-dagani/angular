/**
 * Created by Oliver on 27/02/2017.
 */
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions, Response} from "@angular/http";
import {CookieService} from "angular2-cookie/services/cookies.service";
import {CookieOptionsArgs} from "angular2-cookie/services/cookie-options-args.model";
import {ConnectionAccesPoints} from "./ConnectionAccesPoints";
import {el} from "@angular/platform-browser/testing/browser_util";
import {timeout} from "rxjs/operator/timeout";

@Injectable()
export class AccountService {
  private apiURL = ConnectionAccesPoints._apiURL;
  private _token: string;
  private email: string;
  // private options:CookieOptionsArgs;

  constructor(private http: Http, private cookie: CookieService) {
  }

  register(json,email): Observable<any> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    this.email = email;
    return this.http.post(this.apiURL + "/accounts", json, options).map(res => {
        let token = res.text();
        if (token) {
          this._token = token;
          this.cookie.put('_token', token);
          this.cookie.put('e', this.email);
          console.log(this.cookie.get('e'));
          return token;
        } else return false;
      }
    ).catch(err => Observable.throw(err.status));
  }

  login(json): Observable<boolean> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    this.email = json.emailaddress;
    return this.http.post(this.apiURL + '/accounts/login', json, options)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let token = response.text();
        if (token) {

          // set token property
          this._token = token;
          // localStorage.setItem('_token', JSON.stringify({token: token}));
          this.cookie.put('_token', token);
          this.cookie.put('e', this.email);
          // return true to indicate successful login
          return true;
        } else {
          // return false to indicate failed login
          return false;
        }
      }).catch(this.handleError);
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this._token = null;
    this.email = null;
    //localStorage.removeItem('_token');
    this.cookie.remove('_token');
    this.cookie.remove('e');
  }

  getInfo() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let email = this.cookie.get('e');
    return this.http.get(this.apiURL + "/accounts/" + email, options).map(res => res.json()).catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = "error";
    }
    return Observable.throw(errMsg);
  }

  public get token(): string {
    // let t = JSON.parse(localStorage.getItem('_token'));
    let t = this.cookie.get('_token');
    console.log(t);
    return t;
  }
}
