import { Component, OnInit } from '@angular/core';
import {AccountService} from "../AccountService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  constructor(private ac: AccountService,private router :Router) { }

  ngOnInit() {
  }

  logout(){
    this.ac.logout();
    this.router.navigate(['/']);
  }
}
