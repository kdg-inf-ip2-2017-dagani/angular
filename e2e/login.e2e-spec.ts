import{
  browser, element, by, protractor
}
  from 'protractor';
import { StadsSpelPage } from './app.po';

describe('login', function() {
  let page : StadsSpelPage;

  beforeEach(() => {
    page = new StadsSpelPage();
    page.navigateTo();
  });

  it("should have disabled loginbutton", () => {
    let submit = element(by.name('loginbutton'));
    expect(submit.isEnabled()).toEqual(false);
  });

  it("should enable loginbutton when both fields contain text", () => {
    let loginemail = element(by.name('loginemail'));
    loginemail.sendKeys("default@host.com");
    let loginpassword = element(by.name('loginpassword'));
    loginpassword.sendKeys("test");
    let submit = element(by.name('loginbutton'));
    expect(submit.isEnabled()).toEqual(true);
  });

  it("should navigate to room page", () => {
    let loginemail = element(by.name('loginemail'));
    loginemail.sendKeys("default@host.com");
    let loginpassword = element(by.name('loginpassword'));
    loginpassword.sendKeys("test");
    element(by.name('loginbutton')).click();
    expect(browser.getCurrentUrl()).toMatch("localhost:4200/room");
  });
});



