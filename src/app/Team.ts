import {District} from "./District";
/**
 * Created by Oliver on 15/03/2017.
 */
export class Team {
  private _teamName: string;
  private _players: Array<string>;
  private _color: string;


  constructor(teamName: string, color: string) {
    this._teamName = teamName;
    this._color = color;
    this._players = [];
  }

  addPlayer(p: string) {
    this._players.push(p);
  }

  get name(): string {
    return this._teamName;
  }

  get players(): Array<string> {
    return this._players;
  }

  get color(): string {
    return this._color;
  }
}
