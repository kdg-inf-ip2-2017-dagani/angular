import{
  browser, element, by, protractor
}
  from 'protractor';
import { StadsSpelPage } from './app.po';

describe('login', function() {
  let page: StadsSpelPage;

  beforeEach(() => {
    page = new StadsSpelPage();
    page.navigateTo();
    let loginemail = element(by.name('loginemail'));
    loginemail.sendKeys("default@host.com");
    let loginpassword = element(by.name('loginpassword'));
    loginpassword.sendKeys("test");
    element(by.name('loginbutton')).click();
  });

  it("should have disabled create button", () => {
    let button = element(by.name("createbutton"));
    expect(button.isEnabled()).toEqual(false);
  });

  it("should enable create button when gamename and password are not empty", () => {
    element(by.name("name")).sendKeys("testGame");
    element(by.name("password")).sendKeys("testGame");
    let button = element(by.name("createbutton"));
    expect(button.isEnabled()).toEqual(true);
  });

  it("should navigate to a page with a map", () => {
    element(by.name("name")).sendKeys("testGame");
    element(by.name("password")).sendKeys("testGame");
    element(by.name("createbutton")).click();
    expect(browser.isElementPresent(by.name("map")));
  });
});

