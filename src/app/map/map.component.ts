///<reference path="../../../node_modules/@angular/core/src/metadata/ng_module.d.ts"/>
import {Component, OnInit, CUSTOM_ELEMENTS_SCHEMA, Input, OnChanges, SimpleChanges} from '@angular/core';
import {SebmGoogleMap, LatLngLiteral} from 'angular2-google-maps/core';
import {SebmGoogleMapPolygon, SebmGoogleMapCircle} from 'angular2-google-maps/core/directives';


import {District} from "../District";
import {DistrictService} from "../DistrictService";
import {Point} from "../Point";
import {Player} from "../Player";
//import {Player} from "../Player";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [DistrictService],

})
export class MapComponent implements OnInit {

  @Input() private mes:any;
  title: string = 'map';
  lat: number = 51.163798;
  lng: number = 4.139931;
  zoom:number =15;
  @Input() districts: Array<District>;
  @Input() players: Array<Player>;
  @Input() banken: Array<Point>;
  @Input() tradePost: Array<Point>;

  mapStyle = [
    {
      "featureType": "administrative",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.province",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "on"
        },
        {
          "color": "#e3e3e3"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
        {
          "color": "#cccccc"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "simplified"
        },
        {
          "color": "#000000"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.station.airport",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.station.airport",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
          // "color": "#FFFFFF"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
  ];
//
  constructor() {

  }
  ngOnInit() {

  }

}
