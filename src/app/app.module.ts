import {BrowserModule} from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {RoomComponent} from './room/room.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {TeamComponent} from './team/team.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {GameSettingsComponent} from './game-settings/game-settings.component';
import {MapComponent} from './map/map.component';
import {RouterModule} from "@angular/router";
import {StartComponent} from './start/start.component';
import {AccountComponent} from './account/account.component';
import {StartSettingsComponent} from './start-settings/start-settings.component';
import {AgmCoreModule} from "angular2-google-maps/core";
import {AuthGuard} from "./AuthGuard";
import {AccountService} from "./AccountService";
import {DistrictService} from "./DistrictService";
import {RoomService} from "./RoomService";
import {CookieService} from "angular2-cookie/services/cookies.service";
import {AboutComponent} from "./about/aboutcomponent";

export const AppRoutes = [
  {path: '', component: HomeComponent},
  {path: 'room', component: StartComponent,canActivate: [AuthGuard]},
  {path: 'room/:id', component: RoomComponent,canActivate: [AuthGuard]},
  {path: 'about', component: AboutComponent},
  { path: '**', redirectTo: 'room' }
];
@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    RoomComponent,
    LoginComponent,
    RegisterComponent,
    TeamComponent,
    StatisticsComponent,
    GameSettingsComponent,
    MapComponent,
    StartComponent,
    AccountComponent,
    StartSettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDhFTSexi4nAPDcGdh4Xl1e8HeyFMKLEF8'
    }),
    RouterModule.forRoot(AppRoutes,{useHash:true})
  ],
  providers: [AuthGuard,
    AccountService,DistrictService,RoomService,CookieService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
}


