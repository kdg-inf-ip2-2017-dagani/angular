import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AccountService} from "../AccountService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [AccountService]
})
export class RegisterComponent implements OnInit {
  registergroup: FormGroup;

  constructor(private fb: FormBuilder, private ac_service: AccountService, private router: Router) {
  }

  ngOnInit() {
    this.registergroup = this.fb.group({
      username: ['', Validators.required],
      passwords: this.fb.group({
        password: ['', Validators.required],
        repeat: ['', Validators.required]
      }, {validator: this.areEqual}),
      emailAddress: ['', Validators.required],
      accessLevel: ['HOST']
    });
  }

  areEqual(group: FormGroup) {
    let valid = group.controls['password'].value == group.controls['repeat'].value;
    if (valid) {
      return null;
    }
    return {
      areEqual: true
    };
  }

  private fixJson(json: string, val: string): string {
    json = json.replace('"passwords":{"password":"' + val + '","repeat":"' + val + '"}', '"password":"' + val + '"');
    return json;
  }

  public register({value, valid}:{value: any, valid: boolean}) {
    if (valid) {
      let email = value.emailAddress;
      let json = JSON.stringify(value);
      json = this.fixJson(json, value.passwords.password);
      this.ac_service.register(json,email).subscribe(data => {
          console.log(data);
          this.router.navigate(['/room']);
        }, err => {
          console.log(err);
          if (err == 409) alert('usernaam al in gebruik');
        }
      );
    }
  }

}
