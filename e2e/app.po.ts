import { browser, element, by } from 'protractor';

export class StadsSpelPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}

export class RoomPage {
  navigateTo() {
    return browser.get('/room');
  }
}
