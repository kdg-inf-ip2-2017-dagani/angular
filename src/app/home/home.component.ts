import {Component, OnInit} from '@angular/core';
import {LoginComponent} from "../login/login.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = "Stadsspel Sint-Niklaas";

  constructor(private route:Router) {
  }

  ngOnInit() {
  }
  about(){
    this.route.navigate(['/about/']);
  }

}
