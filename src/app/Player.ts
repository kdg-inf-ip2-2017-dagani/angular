/**
 * Created by Toon Vermeulen on 3/6/2017.
 */
export class Player {
  get lat(): number {
    return this._lat;
  }
  get lng(): number {
    return this._lng;
  }
  get team(): string {
    return this._team;
  }
  set team(value: string) {
    this._team = value;
  }
  get color(): string {
    return this._color;
  }
  set color(value: string) {
    this._color = value;
  }
  get id(): string {
    return this._id;
  }
  set id(value: string) {
    this._id = value;
  }
  get name(): string {
    return this._name;
  }
  set name(value: string) {
    this._name = value;
  }
  private _id:string;
  private _lng:number;
  private _lat:number;
  private _name:string;
  private _color:string;
  private _team:string;


  constructor(id: string) {
    this._id = id;
    // this._lat=0;
    // this._lng=0;
    this._lat=51.164461;
    this._lng= 4.140076;
    this._color="#4bff47"
  }

  update(xlat, ylng) {
    this._lng=ylng;
    this._lat=xlat;
  }



}

