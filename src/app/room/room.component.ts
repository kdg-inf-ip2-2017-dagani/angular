import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RoomService} from "../RoomService";
import {DistrictService} from "../DistrictService";
import {District} from "../District";
import {Point} from "../Point";
import {Player} from "../Player";
import {AccountService} from "../AccountService";
import {isUndefined} from "util";
import {PlayersMap} from "../PlayersMap";
import {ConnectionAccesPoints} from "../ConnectionAccesPoints";
import {Team} from "../Team";

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css'],
  providers: [RoomService, DistrictService]
})
export class RoomComponent implements OnInit {
  private id: any;
  private game: any;
  private token: string;
  private websocket_token: string;
  //@Input() port;
  private ws: WebSocket;
  message: string;
  districts: Array<District> = [];
  playerMap: PlayersMap = new PlayersMap();
  banken: Array<Point> = [];
  tradePost: Array<Point> = [];
  teams: Array<Team> = [];
  heartbeatInterval: any;
  private winner: string = "";
  private running: boolean;


  constructor(private route: ActivatedRoute, private service: DistrictService, private rs: RoomService, private ac: AccountService) {
    // let self = this;
    // service.getDistricten().subscribe(function (response) {
    //   self.districtUpdate(response, self);
    // });
    // service.getBanken()
    //   .subscribe(function (response) {
    //     self.BankenUpdate(response, self);
    //   });
    // service.getTradePost()
    //   .subscribe(function (response) {
    //     self.trandepostUpdate(response, self);
    //   });
    // this.UpdateTeams(rs.getGame(this.route.snapshot.params['id']), this);
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.running = false;
    //  this.playerMap.add(new Player("Toon"));
    //  console.log("per");
    //  console.log(this.playerMap.players);
    // this.playerMap.get("Toon").update(51.168896, 4.137276);
    //  console.log("per");
    //  console.log(this.playerMap.players);
    this.rs.getGame(this.id).subscribe(data => {
      this.game = data;
      console.log(this.game);
      this.updateGane();
    }, err => {
      console.log(err);
    });
    this.token = this.ac.token;
    this.rs.registerSocket(this.id, this.token).subscribe(data => {
      this.websocket_token = data;
      this.setupTCPSocket(this, ConnectionAccesPoints._WsURL);
      console.log(data);
    }, err => {
      console.log(err);
    });
  }

  updateGane() {
    this.BankenUpdate(this.game.banks);
    this.trandepostUpdate(this.game.tradePosts);
    this.districtUpdate(this.game.districts);
    this.districtUpdate(this.game.markets);
    this.UpdateTeams(this.game.teams);
  }

  UpdateTeams(t) {
    console.log(t.length);
    for (let i = 0; i < t.length; i++) {
      let tm = new Team(t[i].teamName, t[i].customColor);
      console.log(t[i].players.length);
      let players = t[i].players;
      let dis = t[i].districts;
      for (let j = 0; j < players.length; j++) {
        tm.addPlayer(players[j]);
      }
      for (let j = 0; j < dis.length; j++) {
        for (let x = 0; x < this.districts.length; x++) {
          if (this.districts[x].id == dis[j].id) {
            this.districts[x].color = t[i].customColor;
          }
        }
      }
      this.teams.push(tm)
    }
    console.log(this.teams);
  }


  trandepostUpdate(myArray) {
    for (let i = 0; i < myArray.length; i++) {
      let b = new Point(myArray[i].type, myArray[i].point.longitude, myArray[i].point.latitude, myArray[i].name);
      this.tradePost.push(b);
    }
  }

  BankenUpdate(myArray) {
    for (let i = 0; i < myArray.length; i++) {
      let b = new Point(myArray[i].type, myArray[i].point.longitude, myArray[i].point.latitude, myArray[i].name);
      this.banken.push(b);
    }
  }


  districtUpdate(districts) {
    for (let i = 0; i < districts.length; i++) {
      let d = new District(districts[i].id, districts[i].name);
      let points = districts[i].points;
      for (let j = 0; j < points.length; j++) {
        if (points[j].type === "COORDINATE") {
          d.addPoint(points[j].latitude, points[j].longitude);
        } else {
          d.setmiddle(points[j].latitude, points[j].longitude);
        }
      }
      this.districts.push(d);
    }
  }

  private setupTCPSocket(self, url) {
    this.ws = new WebSocket(url);
    this.ws.onopen = function () {
      let mw = {
        messageType: "WEB_CONNECT",
        token: self.websocket_token,
        message: "",
        gameID: self.id,
        clientID: ""
      };
      let message = JSON.stringify(mw);
      console.log(mw);
      console.log(message);
      this.send(message);
      self.heartbeatInterval = setInterval(function () {
        let mw = {
          messageType: "HEARTBEAT",
          token: self.websocket_token,
          message: "",
          gameID: self.id,
          clientID: ""
        };

        let message = JSON.stringify(mw);
        self.ws.send(message);
      }, 1000);
    };
    this.ws.onmessage = function (evt) {
      // console.log("t" + evt.data);
      let t = JSON.parse(evt.data);
      switch (t.messageType) {
        case "GAME_START":
          console.log('started the game' + t.message);
          break;
        case "BULK_LOCATION":
          self.updatePlayer(t.message);
          break;
        case "WINNING_TEAM":
          self.showTheWinner(t.message);
          break;
        case "DISTRICT_NOTIFICATION":
          //TODO: nog backend w8 op json:{vorig team,nieuw team,districtid}
          break;
        case "GAME_STOP":
          break;

      }
    };
    this.ws.onclose = function (evt) {
      console.log(evt);

      //
    };
  }

  public updatePlayer(message) {
    let m = JSON.parse(message);
    console.log(m);
    // console.log(message);
    for (let j = 0; j < m.locations.length; j++) {
      let uppers = m.locations[j];
      if (this.playerMap.get(uppers.key)) {
        this.playerMap.get(uppers.key).update(uppers.location.lat, uppers.location.lng);
      } else {
        let p: Player = new Player(uppers.key);
        this.playerMap.add(p);
        this.playerMap.get(uppers.key).update(uppers.location.lat, uppers.location.lng);
      }

    }
  }

  public showTheWinner(winner: string) {
    this.winner = winner;
    this.running = false;

  }

  public stoping(nr: number) {
    this.rs.stopGame(this.id, this.token).subscribe(data => {
      console.log(data);
      clearInterval(this.heartbeatInterval);
    }, err => {
      console.log(err)
    });
  }

  public starting(nr: number) {
    this.game.duration.seconds = nr;
    this.rs.updateGame(this.game, this.token).subscribe(data => {
      // this.game = data;
      console.log(this.game);
      this.updateGane();
    }, err => {
      console.log(err)
    });
    console.log(this.game);
    console.log('starting' + nr);
    this.running = true;
    this.rs.startGame(this.id, this.token).subscribe(data => {
      console.log(data)
    }, err => {
      console.log(err)
    });
  }


}

