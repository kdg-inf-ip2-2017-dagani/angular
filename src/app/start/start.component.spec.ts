/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { StartComponent } from './start.component';
import {StartSettingsComponent} from "../start-settings/start-settings.component";
import {AccountComponent} from "../account/account.component";
import {ReactiveFormsModule, FormBuilder, ControlContainer, FormGroup} from "@angular/forms";

describe('StartComponent', () => {
  let component: StartComponent;
  let fixture: ComponentFixture<StartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartSettingsComponent,AccountComponent,StartComponent ],
      imports:[ReactiveFormsModule],
      providers:[FormBuilder]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
