/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import {StartSettingsComponent} from './start-settings.component';
import {ReactiveFormsModule, FormBuilder, FormGroup, ControlContainer} from "@angular/forms";

import {RoomService} from "../RoomService";
import {Http, BaseRequestOptions, Response} from "@angular/http";
import {MockBackend} from "@angular/http/testing";

describe('StartSettingsComponent', () => {
  let component: StartSettingsComponent;
  let fixture: ComponentFixture<StartSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [StartSettingsComponent],
        imports: [ReactiveFormsModule],
        providers: [RoomService, {
          provide: Http,
          useFactory: (mockBackend, options) => {
            return new Http(mockBackend, options);
          }, deps: [MockBackend, BaseRequestOptions]
        },
          MockBackend, BaseRequestOptions,FormBuilder]
      }
    )
      .compileComponents();
  }));
//TODO: fix problem
  beforeEach(() => {
    fixture = TestBed.createComponent(StartSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  /*it('should create a `Formbuilder` should not be null', () => {
   component.ngOnInit();
   expect(component.fb).not.toBeNull();
   });*/
  // it('should create a `FormGroup` comprised of `FormControl`s', () => {
  //   component.ngOnInit();
  //   expect(component.settingsgroup instanceof FormGroup).toBe(true);
  // });
  // beforeEachProviders(() => [
  //   RoomService,
  //   BaseRequestOptions,
  //   MockBackend,
  //   addProviders(Http, {
  //     useFactory: (backend: MockBackend, defaultOptions: BaseRequestOptions) => {
  //       return new Http(backend, defaultOptions);
  //     },
  //     deps: [MockBackend, BaseRequestOptions]
  //   })
  // ]);
  // beforeEach(inject([MockBackend], (backend: MockBackend) => {
  //   const baseResponse = new Response(new ResponseOptions({body: 1}));
  //   backend.connections.subscribe((c: MockConnection) => c.mockRespond(baseResponse));
  // }));
  // it('should return response when subscribed to creatRoom',
  //   inject([RoomService], (roomService: RoomService) => {
  //    // roomService.createRoom('', 0, 0, '').subscribe((res: Response) => {
  //       expect(res).toBeTruthy();
  //     });
  //   })
  // );
});
