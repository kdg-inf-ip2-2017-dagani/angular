/* tslint:disable:no-unused-variable */

import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {RouterModule, RouterOutlet, Router} from "@angular/router";
import {Renderer} from "@angular/core";

class MockRouter {
  public navigate() {
  };
}

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        Renderer,
        {provide: Router, useClass: MockRouter},
        RouterOutlet,
        RouterModule
      ]
    });

    it('should create the app', async(() => {
      let fixture = TestBed.createComponent(AppComponent);
      let app = fixture.debugElement.componentInstance;
      expect(app).toBeTruthy();
    }));
  })
});
