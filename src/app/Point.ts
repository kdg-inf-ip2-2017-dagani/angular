/**
 * Created by Toon Vermeulen on 3/6/2017.
 */
export class Point {
  private _type:string;
  private _lng:number;
  private _lat:number;
  private _name:string;


  constructor(type: string, lng: number, lat: number, name: string) {
    this._type = type;
    this._lng = lng;
    this._lat = lat;
    this._name = name;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get lng(): number {
    return this._lng;
  }

  set lng(value: number) {
    this._lng = value;
  }

  get lat(): number {
    return this._lat;
  }

  set lat(value: number) {
    this._lat = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}
