import{
  browser, element, by, protractor
}
  from 'protractor';
import { StadsSpelPage } from './app.po';

describe('login', function() {
  let page: StadsSpelPage;

  beforeEach(() => {
    page = new StadsSpelPage();
    page.navigateTo();
    let loginemail = element(by.name('loginemail'));
    loginemail.sendKeys("default@host.com");
    let loginpassword = element(by.name('loginpassword'));
    loginpassword.sendKeys("test");
    element(by.name('loginbutton')).click();
    element(by.name("name")).sendKeys("testGame");
    element(by.name("password")).sendKeys("testGame");
    let button = element(by.name("createbutton"));
    button.click();
  });

  it("should have winner when stopping game", () => {
    element(by.name('startbutton')).click();
    element(by.name('stopbutton')).click();
    //TODO: check if winner is on page
  });

});

