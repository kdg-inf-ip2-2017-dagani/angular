/**
 * Created by Oliver on 28/02/2017.
 */
import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {CookieService} from "angular2-cookie/services/cookies.service";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {ConnectionAccesPoints} from "./ConnectionAccesPoints";

@Injectable()
export class AuthGuard implements CanActivate {
  private apiURL = ConnectionAccesPoints._apiURL;

  constructor(private router: Router, private cookie: CookieService, private http: Http) {
  }

  canActivate() {
    if (this.cookie.get('_token') != null && this.cookie.get('e') != null) {
      // logged in so return true
      let email = this.cookie.get('e');
      if (email != null) {
        this.http.get(this.apiURL + "/accounts/" + email).map(this.redirect).catch(this.redirect);
        return true;
      }
    }

    // not logged in so redirect to login page
    this.router.navigate(['/']);
    return false;
  }

  private redirect(res: Response| any) {
    console.log('res: ' + res);
    if (res instanceof Response) {
      if (res.status === 404) {
        this.router.navigate(['/']);
      }
    }
    return Observable.throw('error');
  }
}
