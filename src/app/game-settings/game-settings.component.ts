import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder} from "@angular/forms";
@Component({
  selector: 'app-game-settings',
  templateUrl: './game-settings.component.html',
  styleUrls: ['./game-settings.component.css']
})
export class GameSettingsComponent implements OnInit {
  @Output() startEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() stopEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Input() running: boolean;
  duration: number = 50;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
  }

  public start() {
    console.log('clicked start');
    let nr = this.duration * 60;
    this.startEmitter.emit(nr);
  }

  public stop() {
    console.log('clicked stop');
    let nr = this.duration * 60;
    this.stopEmitter.emit(nr);
  }

}
