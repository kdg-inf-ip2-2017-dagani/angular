/**
 * Created by Toon Vermeulen on 3/13/2017.
 */


export class ConnectionAccesPoints {
  // public static _apiURL = "http://10.134.229.40:8090/api";
  public static _apiURL = "https://stniklaas-stadsspel.herokuapp.com/api";
  // public static _apiURL = "http://10.134.229.38:8090/api";
  public static _WsURL = "wss://stniklaas-stadsspel.herokuapp.com/user";
}
