/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {RoomComponent} from './room.component';
import {TeamComponent} from "../team/team.component";
import {StatisticsComponent} from "../statistics/statistics.component";
import {MapComponent} from "../map/map.component";
import {GameSettingsComponent} from "../game-settings/game-settings.component";
import {RoomService} from "../RoomService";

describe('RoomComponent', () => {
  let component: RoomComponent;
  let fixture: ComponentFixture<RoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoomComponent, TeamComponent, StatisticsComponent, MapComponent, GameSettingsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
